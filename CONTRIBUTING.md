# Contribute To Hellscape

Community made patches, bug reports and contributions are always welcome!

When contributing please ensure you follow the guidelines below so that we can
keep on top of things.

## Getting Started

* Submit a ticket for your issue, assuming one does not already exist.
  * Raise it on our [Issue Tracker]
  * Clearly describe the issue including steps to reproduce the bug.
  * Make sure you fill in the earliest version that you know has the issue as
    well as the version of 7 Days to Die you're using.

## Making Changes

* Fork the repository on GitLab
* Make the changes to your forked repository
* When committing, reference your issue (if present) and include a note about
  the fix
* If possible, and applicable, please also add/update unit tests for your changes
* Push the changes to your fork and submit a merge request to the 'master' branch
  of the Hellscape repository

## Code Documentation

* Please make sure that you document your code following the examples in the
  existing codebase!

At this point you're waiting on us to merge your request. We'll review all
merge requests, and make suggestions and changes if necessary.

### Additional Resources

* [General GitLab Documentation]
* [GitLab Merge Request Documentation]

[Issue Tracker]: https://gitlab.com/widgitlabs/7daystodie/hellscape/issues
[General GitLab Documentation]: https://docs.gitlab.com/ee/user/
[GitLab Merge Request Documentation]: https://docs.gitlab.com/ee/user/project/merge_requests/index.html#merge-requests
