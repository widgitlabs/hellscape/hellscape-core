using System.Reflection;

namespace Harmony
{
    public class Hellscape : IModApi
    {
        public void InitMod(Mod mod)
        {
            var harmony = new HarmonyLib.Harmony(GetType().ToString());

            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }

    }
}
