using HarmonyLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Hellscape.Harmony
{
    [HarmonyPatch(typeof(ItemActionCollectWater), nameof(ItemActionCollectWater.ExecuteAction))]
    public class PatchWaterSources
    {
        public static void Postfix(ItemActionCollectWater __instance, string ___soundStart, string ___changeItemToItem, ItemActionData _actionData, bool _bReleased)
        {
            if (!_bReleased)
            {
                return;
            }

            ItemInventoryData invData = _actionData.invData;

            if (Voxel.Raycast(invData.world, invData.hitInfo.ray, Constants.cDigAndBuildDistance, Voxel.HM_All, 0f) && Voxel.voxelRayHitInfo.bHitValid)
            {
                List<BlockValue> focusedBlocks = new();
                World world = _actionData.invData.world;
                HitInfoDetails hitInfo = Voxel.voxelRayHitInfo.hit;

                int count = 1;
                while (__instance.Properties.Values.ContainsKey("Focused_blockname_" + count))
                {
                    string blockName = __instance.Properties.Values["Focused_blockname_" + count];
                    BlockValue blockValue = ItemClass.GetItem(blockName).ToBlockValue();

                    if (blockValue.Equals(BlockValue.Air))
                    {
                        throw new Exception($"Unknown block name '{blockName}' in use_action!");
                    }

                    focusedBlocks.Add(blockValue);
                    count++;
                }

                if (focusedBlocks.Any(x => x.Equals(hitInfo.blockValue)))
                {
                    _actionData.lastUseTime = Time.time;
                    invData.holdingEntity.RightArmAnimationUse = true;

                    if (___soundStart != null)
                    {
                        invData.holdingEntity.PlayOneShot(___soundStart);
                    }

                    ThreadManager.StartCoroutine(ExchangeItem(___changeItemToItem, __instance, _actionData));
                }
            }
        }

        private static IEnumerator ExchangeItem(string itemName, ItemAction itemAction, ItemActionData actionData)
        {
            if (!string.IsNullOrEmpty(itemName))
            {
                ItemValue itemValue = ItemClass.GetItem(itemName);
                yield return new WaitUntil(() => !itemAction.IsActionRunning(actionData));

                actionData.invData.holdingEntity.inventory.SetItem(actionData.invData.slotIdx, new ItemStack(itemValue, actionData.invData.holdingEntity.inventory.holdingCount));
            }
        }
    }
}
