# Hellscape - A 7 Days to Die Overhaul Mod

<!-- markdownlint-disable MD013 -->
[![License: GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue.svg)][License]
[![Open Issues](https://img.shields.io/gitlab/issues/open-raw/widgitlabs/7daystodie/hellscape-core?gitlab_url=https%3A%2F%2Fgitlab.com%2F)][Open Issues]
[![Pipelines](https://gitlab.com/widgitlabs/7daystodie/hellscape-core/badges/main/pipeline.svg)][Pipelines]
[![Discord](https://img.shields.io/discord/586467473503813633?color=899AF9)][Discord]
<!-- markdownlint-restore MD013 -->

Hellscape started because I like the idea of overhauls like Undead Legacy and
Darkness Falls, but I want a pretty UI (by my standards) and have a hundred
changes I want to make on top of either. The original idea was to build it as a
UI-independent gameplay and feature overhaul, but we've since added an official
(but optional) UI of our own, as well as a custom world complete with lore
and a full backstory! Hellscape is also a work in progress - expect frequent
updates that may or may not break existing saves.

## Versioning

I know this part is boring, but I promise it's important. If you ask for
support and the answer is "you didn't pay attention and have to restart with
this release" we'll send the horde after you.

We use [semantic versioning] because it makes things
simple. Versions consist of three numbers (from left to right): major version,
minor version, and patch. When the first number (the **major version**) is
increased, it indicates that the release contains backward-incompatible changes
which will require a game restart. When the major version is increased, the
minor version and patch are reset to zero. When the second number (the
**minor version**) is increased, it indicates that the release contains
backward-compatible changes which will not require a game restart. When the
third number (the **patch**) is increased, it indicates that the release
contains only bug fixes.

_**Note:** Until Hellscape reaches version 1.0.0, it is in rapid development and
we are treating versioning slightly differently. Changes to the **minor
version** will indicate that a game restart is required. Changes to the
**patch** will not require a game restart._

## Features

Hellscape UI and our custom map are optional but highly recommended. Hellscape
is currently tested with Hellscape UI, vanilla, and SMX

### Hellscape

* Added working and powered versions of doors in the base game that aren't
  player craftable
* Added a working version of the wood-burning stove
* Added numerous recipes for non-craftable items in the base game
* Added several paint textures that aren't available in the base game
* Minor changes to skill trees
* Made it possible to pick up traps (assuming they aren't damaged)
* Adjustments to many scrap recipes to make them more consistent or believable
* Adjustments to many crafting recipes to make them more consistent or
  believable
* Adjustments to speed and damage profiles of many traps and weapons to more
  closely match their real-world values
* Adjustments to the ammo used by several weapons to better distribute ammo use
* Re-added grain alcohol with a potentially lethal side effect
* Added charcoal as a cheap and dirty alternative to coal
* Added charcoal to the charcoal grill loot list
* Made vending machines and ice machines lootable with custom loot lists
* Added bags of ice and associated recipes
* Tougher zombies
* Bonuses for exploring out at night
* Adjustments to interior cell brightness
* Re-balance animals and zombies

### Hellscape UI

* Custom main menu
* Custom loading screens
* Custom compass
* Custom tooltips
* More coming soon!

### Hellscape World

* Custom a20 map generated with Teragon
* Custom POIs
* Lore coming soon!

## Installation

### Install from Nexus

We currently only _officially_ support manual installation. Currently,
Hellscape works just fine if installed through Vortex, but I don't expect that
to be the case when we get past all the basic XML tweaks and such and get to
the point of doing stuff with Harmony.

### Install from GitLab

You can download any version of Hellscape from the [Releases page].

## Terms of Use

The Hellscape codebase is and will remain, open source. That said, some of
the graphical assets are currently licensed to us, or are used under other
open-source licenses. We don't have a full-time designer on staff at the
moment, so it's an unfortunate necessity.

## Credits

Hellscape wouldn't be possible without the work of a few others.

Read [CREDITS.md] for the current list.

[License]: https://gitlab.com/widgitlabs/7daystodie/hellscape-core/blob/main/license.txt
[Open Issues]: https://gitlab.com/widgitlabs/7daystodie/hellscape-core/-/boards
[Pipelines]: https://gitlab.com/widgitlabs/7daystodie/hellscape-core/pipelines
[Discord]: https://discord.gg/jrydFBP
[semantic versioning]: https://semver.org
[Releases page]: https://gitlab.com/widgitlabs/7daystodie/hellscape-core/-/releases
[CREDITS.md]: https://gitlab.com/widgitlabs/7daystodie/hellscape-core/-/blob/main/CREDITS.md
