# Hellscape - A 7 Days to Die Overhaul Mod

<!-- markdownlint-disable MD013 -->
[![License: GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue.svg)][License]
[![Open Issues](https://img.shields.io/gitlab/issues/open-raw/widgitlabs/7daystodie/hellscape?gitlab_url=https%3A%2F%2Fgitlab.com%2F)][Open Issues]
[![Pipelines](https://gitlab.com/widgitlabs/7daystodie/hellscape/badges/main/pipeline.svg)][Pipelines]
[![Latest Release](https://img.shields.io/gitlab/v/release/widgitlabs/7daystodie/hellscape?sort=semver)][Latest Release]
[![Discord](https://img.shields.io/discord/586467473503813633?color=899AF9)][Discord]
<!-- markdownlint-restore MD013 -->

Hellscape started because I like the idea of overhauls like Undead Legacy and
Darkness Falls, but I want a pretty UI (by my standards) and have a hundred
changes I want to make on top of either. The original idea was to build it as a
UI-independent gameplay and feature overhaul, but we've since added an official
(but optional) UI of our own, as well as a custom world complete with lore
and a full backstory! Hellscape is also a work in progress - expect frequent
updates that may or may not break existing saves.

## Versioning

I know this part is boring, but I promise it's important. If you ask for
support and the answer is "you didn't pay attention and have to restart with
this release" we'll send a horde after you.

We use [semantic versioning] because it makes things simple. Versions consist
of three numbers (from left to right): major version, minor version, and patch.
When the first number (the **major version**) is increased, it indicates that
the release contains backward-incompatible changes which will require a game
restart. When the major version is increased, the minor version and patch are
reset to zero. When the second number (the **minor version**) is increased, it
indicates that the release contains backward-compatible changes which will not
require a game restart. When the third number (the **patch**) is increased, it
indicates that the release contains only bug fixes.

_**Note:** Until Hellscape reaches version 1.0.0, it is in rapid development and
we are treating versioning slightly differently. Changes to the **minor
version** will indicate that a game restart is required. Changes to the
**patch** will not require a game restart._

## Hellscape Flavors

Hellscape is modular and available in several flavors. _Core_ is the basic
Hellscape overhaul. While _Hellscape UI_ is still in development, we wanted to
leave choice of UI up to the users. We aren't really fans of vanilla, but it
works fine, and [SMX] is an amazing UI with which we maintain compatibility.
Similarly, _Hellscape World_ is both still under development, and completely
optional. We also provide a pre-made all-in-one bundle for those who want
the complete experience. The table blow indicates which Hellscape modules are
included in each flavor.

| | Hellscape Core | Hellscape UI | Hellscape World | Hellscape Bundle |
| ----------- | ----------- | ----------- | ----------- | ----------- |
| 0-Hellscape | Yes | Yes | Yes | Yes |
| Hellscape Create | Yes |  No | No | Yes |
| Hellscape Gear | Yes | No | No | Yes |
| Hellscape Survival | Yes | No | No | Yes |
| Hellscape UI | No | Yes | No | Yes |
| Hellscape Workstations | Yes | No | No | Yes |
| Hellscape World | No | No | Yes | Yes |

## Hellscape Modules

<!-- markdownlint-disable MD013 -->
| Module | Description |
| ----------- | ----------- |
| 0-Hellscape | The core of the Hellscape; required to use any of the included modules. |
| Hellscape Create | Provides addition of and adjustments to decor and building blocks. |
| Hellscape Gear | Overhauls all things gear: clothing, armor, tools, weapons, etc. |
| Hellscape Survival | Overhauls anything that falls into the survival category. |
| Hellscape UI | Our (upcoming) custom UI |
| Hellscape Workstations | Provides adjustments to existing workstations, and addition of new workstations. |
| Hellscape World | Our (upcoming) custom world and lore. |
<!-- markdownlint-enable MD013 -->

## Installation

### Install from Nexus

We currently only _officially_ support manual installation. Currently,
Hellscape works just fine if installed through Vortex, but I don't expect that
to be the case forever.

### Install from GitLab

You can download any version of Hellscape from the [Releases page].

## Terms of Use

The Hellscape codebase is and will remain, open source. That said, some of
the graphical assets are currently licensed to us, or are used under other
open-source licenses. We don't have a full-time designer on staff at the
moment, so it's an unfortunate necessity.

## Credits

Hellscape wouldn't be possible without the work of a few others.

Read [CREDITS.md] for the current list.

[License]: https://gitlab.com/widgitlabs/7daystodie/hellscape/blob/main/license.txt
[Open Issues]: https://gitlab.com/widgitlabs/7daystodie/hellscape/-/boards
[Pipelines]: https://gitlab.com/widgitlabs/7daystodie/hellscape/pipelines
[Latest Release]: https://img.shields.io/gitlab/v/release/widgitlabs/7daystodie/hellscape?sort=semver
[Discord]: https://discord.gg/jrydFBP
[semantic versioning]: https://semver.org
[SMX]: https://www.nexusmods.com/7daystodie/mods/22
[Releases page]: https://gitlab.com/widgitlabs/7daystodie/hellscape/-/releases
[CREDITS.md]: https://gitlab.com/widgitlabs/7daystodie/hellscape/-/blob/main/CREDITS.md
