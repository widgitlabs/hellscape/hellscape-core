# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog],

## [Unreleased] - TBD

### Added

- Empty can and associated recipes.
- Empty glass jar and associated recipes.
- Various functional beds and associated recipes.
- Variant helper for new lanterns.
- Variant helper for new flashlights.
- Improved cover for Designers Delight.

### Fixed

- Night's Embrace buff for a21 changes.
- Sort order for wood burning stove.

### Removed

- Harmony location name patch.
- Most existing changes to progression.xml.

### Updated

- Project structure for a more modular approach.
- Item modifiers for a21 changes.
- Recipes for a21 changes.
- Loot tables for a21 changes.
- Items for a21 changes.
- Blocks for a21 changes.
- ModInfo format to v2.

## [0.0.1] - 2023-05-27

### Added

- Last stable WIP for a20

[Keep a Changelog]: https://gitlab.com/widgitlabs/7daystodie/hellscape/issues
[Unreleased]: https://gitlab.com/widgitlabs/7daystodie/hellscape/-/compare/0.0.1...main
[0.0.1]: https://gitlab.com/widgitlabs/7daystodie/hellscape/-/tags/0.0.1
