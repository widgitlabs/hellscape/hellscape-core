# Hellscape Credits

In addition to those members of the Widgit team who have been involved to
varying degrees, Hellscape wouldn't be possible without the work of a few
others.

## The Fun Pimps

OK, TFP has absolutedly nothing to do with Hellscape, but Hellscape wouldn't
exist without the amazing work they have done to date and continue to do.
7 Days to Die is a great game in its own right, we're just trying to make
it that much better.

## Guppy's Unofficial 7DtD Modding Server

[Guppy's Discord server] has been an undenyable source of help and inspiration
throughout the process of building Hellscape. If you aren't already there...
why not?

## Props

Some people are influential enough to deserve a personal shout-out.

* [khzmusik] - When I started modding 7 Days, this was my reference. My sincere
  thanks for making your work available under the public domain. I wish more
  people were like you.

* Zilox - Gave me a huge push in the right direction to patching
  `Focused_blockname` back in after the a21 update.

## Open License Assets

The following assets or code are included under an open license and may
be reused according to their individual licenses.

* **[Metal Tool Box Photoscan PBR]** by Kless Gyzen
  (Licensed under [Creative Commons Attribution]).

## Commercial License Assets

The following assets are included under a commercial license and may not be re-used.

* **[Bag of Ice]** by Marc Wheeler

[Guppy's Discord server]: https://discord.gg/3aWeEGrjJK
[khzmusik]: https://gitlab.com/karlgiesing/7d2d-a20-modlets
[Metal Tool Box Photoscan PBR]: https://skfb.ly/onwEX
[Creative Commons Attribution]: http://creativecommons.org/licenses/by/4.0/
[Bag of Ice]: https://sketchfab.com/3d-models/bag-of-ice-f8153fcc30b04255aa1a7f2a190660e8
